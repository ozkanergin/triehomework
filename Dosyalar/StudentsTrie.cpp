#include <fstream>
#include <iostream>
#include <string>
using namespace std;
#include "StudentsTrie.h"

using namespace std;

TrieNode::TrieNode(const char& digit) {
	this->digit = digit;
	for (int i = 0; i < MAX_CHILDREN; children[i++] = NULL);
};

// Construct a StudentsTrie using the records in 'file_name' 
StudentsTrie::StudentsTrie(const string& file_name) {
	ifstream student_ids_file(file_name);   // ifstream object for input file
	string current_student_id;

	root = new TrieNode('r');
	root->visited	 = false;
	root->last_point = false;
	temp_root = root;

	if (student_ids_file.is_open()) {
		for (int i = 1; student_ids_file >> current_student_id; i++) {
			insert_id(current_student_id); // YOU HAVE TO IMPLEMENT insert_id() METHOD
		}
		student_ids_file.close();
	}
	else {
		cout << "File: " << file_name << " could NOT be opened!!";
		exit(1);
	}
	student_ids_file.close();
};

// Insert a student ID into the StudentsTrie 
void StudentsTrie::insert_id(const string& student_id) {
	 TrieNode *temporary = root;

	for (int i = 0; i < student_id.length(); i++)
	{
		string indeks;
		indeks.push_back(student_id[i]);
		int index = stoi(indeks);

		if (!temporary->children[index])
			temporary->children[index] = new TrieNode(indeks[0]);

		temporary = temporary->children[index];
	}
	temporary->last_point = true;
};
// Print Student IDs in Trie in ascending order 
void StudentsTrie::print_trie() {
	TrieNode *temproray_root = temp_root;
	for (int i = 0; i <= 9; i++)
	{
		if (temproray_root->last_point == true) 
		{
			if (number == "") break;
			all_numbers.push_back(number);

			while (true)
			{
				if (stack_visiting.size() == 0)
				{
					temp_root = root;
					print_trie();
					break;
				}
				TrieNode* pop_node = stack_visiting.top();
				bool added = true;
				for (int i = 0; i <= 9; i++)
				{
					if (pop_node->children[i] != NULL && pop_node->children[i]->visited == false)
					{
						temp_root = pop_node->children[i];
						temp_root->visited = true;
						stack_visiting.push(temp_root);
						number.push_back(temp_root->digit);
						added = false;
						break;
					}
				}

				if(true == added) 
				{
					number.erase(number.size() - 1);
					stack_visiting.pop();
				}
				else 
				{
					break;
				}
			}
			print_trie();
		}

		if (temproray_root->children[i] != NULL && temproray_root->children[i]->visited == false)
		{
			stack_visiting.push(temproray_root->children[i]);
			temproray_root->children[i]->visited = true;
			number.push_back(temproray_root->children[i]->digit);
			temproray_root = temproray_root->children[i];
			i = -1;
		}
	}
}

// StudentsTrie Destructor
StudentsTrie::~StudentsTrie() {
	for (int i = 0; i < MAX_CHILDREN; i++) {
		if (root->children[i]) delete root->children[i];
	};
};

